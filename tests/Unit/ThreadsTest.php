<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Threads;
use App\Channel;

class ThreadsTest extends TestCase
{
use DatabaseMigrations;

  public function test_a_thread_has_replies()
  {
      $thread = factory('App\Threads')->create();

      $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $thread->replies);
  }


  public function test_a_thread_has_a_creator()
  {
    $thread = factory('App\Threads')->create();

    $this->assertInstanceOf('\App\User', $thread->creator);
  }

  public function test_a_thread_can_add_a_reply()
  {
    $thread = factory('App\Threads')->create();

    $thread->addReply([
        'body'=> 'Foobar',
        'user_id' => 1
    ]);

    $this->assertCount(1, $thread->replies);

  }

  public function test_a_thread_belongs_to_a_channel()
  {
    $thread = factory('App\Threads')->create();

    $this->assertInstanceOf('App\Channel', $thread->channel);
  }

  public function test_a_thread_make_a_string_path()
  {
    $thread = factory('App\Threads')->create();

    $this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}" , $thread->path());
  }
}
