<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChannelTest extends TestCase
{
  use DatabaseMigrations;

  function test_a_channel_consists_of_threads()
  {
      $channel = factory('App\Channel')->create();
      $thread = factory('App\Threads')->create(['channel_id'=> $channel->id]);
      $this->assertTrue($channel->threads->contains($thread));

  }
}
