<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RepliesTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_reply_has_an_owner(){

        $replies = factory('App\Replies')->create();

        $this->assertInstanceOf('App\User', $replies->owner);
    }
}
