<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use DatabaseMigrations;


    public function test_a_user_may_has_a_profile()
    {
        $user = factory('App\User')->create();
       // $this->be($user);

        //$profile = factory('App\Profile')->create();
        $this->get("/profiles/{$user->name}")->assertSee($user->name);
    }
}
