<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Threads;
use Tests\TestCase;

class ParticipateInForumTest extends TestCase
{
    use DatabaseMigrations;

     public function test_an_non_authenticated_user_may_not_reply()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        //$this->withoutExceptionHandling();
        $this->post('/threads/name/1/replies', [])->assertRedirect('/login');
    }


    public function test_a_authenticated_user_may_participate_in_forum()
    {
        $user = factory('App\User')->create();

        $this->be($user);

        $thread = factory('App\Threads')->create();
        $replies = factory('App\Replies')->make();

        $this->post($thread->path() . '/replies', $replies->toArray());
        $this->get($thread->path())
        ->assertSee($replies->body);



    }

    public function test_a_reply_requires_a_body()
    {
        $this->withExceptionHandling();
        $user = factory('App\User')->create();
        $thread = factory('App\Threads')->create();
        $replies = factory('App\Replies')->make(['body'=> null]);

        $this->be($user);

        $this->post($thread->path().'/replies', $replies->toArray())
           ->assertSessionHasErrors('body');

    }
}
