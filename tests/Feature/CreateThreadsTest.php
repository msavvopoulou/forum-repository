<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Testing\TestResponse ;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;
    public function test_an_authenticated_user_can_create_a_thread()
    {
        $this->actingAs(factory('App\User')->create());

        $thread = factory('App\Threads')->create();

        $response =  $this->post('/threads', $thread->toArray());

        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }

    public function test_a_guest_may_not_create_a_thread()
    {
        $this->withExceptionHandling();

        $this->get('/threads/create')
            ->assertRedirect('/login');


        $this->post('/threads')
            ->assertRedirect('/login');

    }

    function test_a_thread_has_a_title()
    {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');

    }

    public function publishThread($overrides = [])
    {
        $this->withExceptionHandling();
        $this->actingAs(factory('App\User')->create());

        $thread = factory('App\Threads')->make($overrides);

        return $this->post('/threads' , $thread->toArray());


    }

    public function test_unauthorized_users_can_not_delete_threads()
    {
        $this->withExceptionHandling();
        $thread = factory('App\Threads')->create();

        $response =$this->delete($thread->path());

        $response->assertRedirect('/login');

        $this->actingAs(factory('App\User')->create());
        $this->delete($thread->path())->assertStatus(403);

    }
    public function test_a_thread_can_be_deleted()
    {
        $this->be(factory('App\User')->create());

        $thread = factory('App\Threads')->create(['user_id' => auth()->id()]);
        $replies = factory('App\Replies')->create(['thread_id'=>$thread->id]);

        $response =$this->json('DELETE', $thread->path());

        $response->assertStatus(204);

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies',['id' => $replies->id]);

    }

    function test_a_thread_has_a_body()
    {
        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');

    }

    function test_a_thread_requires_a_channel()
    {
        factory('App\Channel' , 2)->create();
        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

    }

}
