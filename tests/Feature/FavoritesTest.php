<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    use DatabaseMigrations;

    function test_a_non_authentticated_user_can_favorite_anything()
         {
             $this->withExceptionHandling();
             $this->post('/replies/1/favorites')
                ->assertRedirect('login');

         }


   public function test_an_authenticated_user_can_favorite_any_reply()
     {

         $user = factory('App\User')->create();
         $this->be($user);

         $replies = factory('App\Replies')->create();

         $this->post('/replies/' . $replies->id . '/favorites');

         $this->assertCount(1, $replies->favorites);
    }

    public function test_a_user_may_only_favorite_a_reply_once()
    {
        $user = factory('App\User')->create();
        $this->be($user);

        $replies = factory('App\Replies')->create();

        try {
            $this->post('/replies/' . $replies->id . '/favorites');
            $this->post('/replies/' . $replies->id . '/favorites');
        }catch(\Exception $e){
            $this->fail('Did not expect to insert the same record set twice');
        }
        $this->assertCount(1, $replies->favorites);
    }
 }


