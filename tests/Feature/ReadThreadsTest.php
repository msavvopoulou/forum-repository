<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class   ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->thread = factory('App\Threads')->create();
    }

    public function test_a_user_can_view_all_threads()
    {


        $response = $this->get('/threads');
        $response->assertSee($this->thread ->title);

    }

    public function test_a_user_can_read_single_thread()
    {


        $response = $this->get($this->thread->path());
        $response->assertSee($this->thread->title);

    }

    public function test_a_user_can_read_replies_that_are_associated_with_a_thread()
    {
        $reply = factory('App\Replies')->create(['thread_id'=> $this->thread->id]);

        $response = $this->get($this->thread->path());
        $response->assertSee($reply->body);

    }

    function test_a_user_can_filter_threads_according_to_a_channel()
    {
        $channel = factory('App\Channel')->create();
        $threadInChannel = factory('App\Threads')->create(['channel_id' => $channel->id]);
        $threadNotInChannel = factory('App\Threads')->create();

        $this->get('/threads/'. $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);

    }

    function test_a_user_can_filter_threads_by_any_username()
    {
        $user = factory('App\User')->create(['name' => 'marina']);

        $this->be($user);

        $threadbyMarina = factory('App\Threads')->create(['user_id' => auth()->id()]);

        $threadbyNotMarina = factory('App\Threads')->create();

        $this->get('threads?by=marina')
            ->assertSee($threadbyMarina->title)
            ->assertDontSee($threadbyNotMarina->title);

    }






}
