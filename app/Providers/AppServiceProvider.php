<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\View\View;

use Illuminate\Support\ServiceProvider;
use League\Flysystem\Adapter\Local;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       // View::share('channels', \App\Channel::all());
    }
}
