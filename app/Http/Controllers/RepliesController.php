<?php

namespace App\Http\Controllers;

use App\Replies;
use App\Threads;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request  $request, $channelId , Threads $thread)
    {
        $data = $this->validate($request , [
            'body' => 'required',
        ]);

        $data += ['user_id'=> auth()->id()];

        $thread->addReply($data);

        return redirect($thread->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Replies  $replies
     * @return \Illuminate\Http\Response
     */
    public function show(Replies $replies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Replies  $replies
     * @return \Illuminate\Http\Response
     */
    public function edit(Replies $replies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Replies  $replies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Replies $replies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Replies  $replies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Replies $replies)
    {
        //
    }
}
