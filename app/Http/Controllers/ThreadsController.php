<?php

namespace App\Http\Controllers;
use App\User;
use App\Channel;
use App\Threads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ThreadsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($channelSlug = null)
    {
        if($channelSlug)
        {
            $channelId = Channel::where('slug', $channelSlug)->first()->id;
            $threads = Threads::where('channel_id', $channelId)->latest();
        }
        else
        {
            $threads =Threads::latest();
        }

        if($username = request('by'))
        {
            $user = \App\User::where('name', $username)->firstOrFail();

            $threads = Threads::where('user_id' , $user->id);
        }

        $threads = $threads->get();

        return view('threads.index' , compact('threads'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request ,[
            'title' => 'required',
            'body' => 'required',
            'channel_id'=> 'required'

        ]);

        $thread =Threads::create([

            'user_id'=>auth()->id(),
            'channel_id'=>request('channel_id'),
            'title'=>request('title'),
            'body'=>request('body')
        ]);

        return redirect($thread->path());

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Threads  $threads
     * @return \Illuminate\Http\Response
     */
    public function show($channelId ,Threads $thread)
    {
        return view('threads.show', [
            'thread' => $thread,
            'replies' => $thread->replies()->paginate(1)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Threads  $threads
     * @return \Illuminate\Http\Response
     */
    public function edit(Threads $threads)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Threads  $threads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Threads $threads)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Threads  $threads
     * @return \Illuminate\Http\Response
     */
    public function destroy($channelId, Threads $thread)
    {


        $this->authorize('update', $thread);

        $thread->delete();

        if (request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/threads');
    }

}
