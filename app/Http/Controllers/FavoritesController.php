<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Replies;
use App\Threads;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store( Replies $replies)
    {
        return $replies->favorite();

        // Favorite::create([
        //     'user_id' => auth()->user()->id,
        //     'favorited_id' => $replies->id,
        //     'favorited_type' => get_class($replies)
        // ]);

    }
}
