<?php

namespace App;

use App\Channel;
use App\Replies;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\FactoryBuilder;

class Threads extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($thread){
            $thread->replies()->delete();
        });
    }

    public function path(){

        return "/threads/{$this->channel->slug}/{$this->id}";
    }


    public function replies(){
        return $this->hasMany(Replies::class , 'thread_id', 'id')->withCount('favorites');
    }

    // public function getReplyCountAttribute()
    // {
        // return $this->replies()->count();
    // }


    public function creator(){

        return $this->belongsTo(User::class, 'user_id');
    }

    public function addReply($reply)
    {
       $attribute = $this->replies()->create($reply);

    //    dd($attribute);

    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
