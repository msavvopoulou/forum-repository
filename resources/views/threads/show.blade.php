@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="level">
                        <span class="flex">
                            <a href="/profiles/{{$thread->creator->name}}">{{$thread->creator->name}}</a> posted:
                            {{$thread->title}}
                        </span>

                        @can('update', $thread)
                            <form action="{{$thread->path()}}" method="POST">
                            @csrf
                            {{method_field('DELETE')}}

                            <button type="submit">Delete the Thread</button>
                            </form>
                        @endcan

                    </div>
                </div>

                    <div class="card-body">
                        {{$thread->body}}
                    </div>
            </div>
                <hr>

                @foreach ($thread->replies as $reply)

                @include('threads.replies')


                @endforeach

                    {{$replies->links()}}

                @if (auth()->check())
                <form action="{{$thread->path() . '/replies'}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <textarea name="body" id="body" placeholder="Have something to say?" rows="5" class="form-control"  cols="50" ></textarea>
                    </div>
                    <button class="bg-blue-400" type="submit">Post</button>
                </form>
                @else
                <p class="row justify-content-center">Please <a class="mr-1 ml-1" href="{{route('login')}}"> sign in</a> to participate in the discussion! </p>

                @endif
        </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        This thread was published {{$thread->created_at->diffForHumans()}} by
                        <a href="/">{{$thread->creator->name}}</a> and currently has {{$thread->replies()->count()}} {{Str::plural('reply', $thread->replies()->count())}}.


                    </div>
                </div>
            </div>

    </div>
</div>



@endsection
