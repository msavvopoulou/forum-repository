<div class="card">
    <div class="card-header">
        <div class="level">
            <h5 class="flex">
                <a href="/profiles/{{$reply->owner->name}}">
                    {{$reply->owner->name}}
                </a>
                said {{$reply->created_at->diffForHumans()}}
            </h5>
                <form action="/replies/{{$reply->id}}/favorites" method="POST">
                    @csrf
                    <button type="submit">{{$reply->favorites()->count()}} {{Str::plural('Favorite', $reply->favorites()->count())}} </button>
                </form>
        </div>
    </div>
        <div class="card-body">{{$reply->body}}</div>
</div>

<hr>