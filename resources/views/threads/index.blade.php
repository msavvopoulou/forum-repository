@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Forum for threads</div>
                @foreach ($threads as $thread)
                <div class="card-body">
                    <article>
                        <div class="level">
                            <h4 class="flex">
                                <a href="{{$thread->path()}}">
                                    {{$thread->title}}
                                </a>
                            </h4>

                        <a href="{{$thread->path()}}">{{$thread->replies()->count()}} {{Str::plural('reply', $thread->replies()->count())}}</a>
                        </div>
                            <div class="body">
                            {{$thread->body}}
                        </div>
                        <hr>
                    </article>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
